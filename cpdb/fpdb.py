from __future__ import print_function

import bdb
import os
import functools
import linecache
import sys
import threading
import logging
import traceback
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from cpdb import pdb
from tempfile import NamedTemporaryFile
from pprint import pprint, pformat
import xml.dom.minidom
import inspect

# TODO make these configurable
breakpoints_file = os.path.expanduser("~/.cpdb/breakpoints")
# editor command to launch your editor, this should be non blocking(preferably)
editor_cmd = "xdev --fileline %s:%s"
pprint_editor_cmd = "vim %s"
diff_cmd = "vimdiff %s %s"

class BreakPointFileModifiedEventHandler(FileSystemEventHandler):
    def __init__(self, debugger, breakpoints_file=breakpoints_file):
        self.debugger = debugger
        self.breakpoints_file = breakpoints_file
        self.prev_event = None

    def on_modified(self, event):
        try:
            skip = False
            if event.src_path == self.breakpoints_file and not skip:
                self.prev_event = event
                print(event)
                self.debugger.load_breakpoints(filename=self.breakpoints_file)
        except Exception as e:
            print(e)



class FPdb(pdb.Pdb):
    threadmap = {}
    skipfirst = True
    def __init__(self, *args, **kwargs):
        super(FPdb, self).__init__(*args, **kwargs)
        self.botframe = None
        self._wait_for_mainpyfile = False
        self.follow = False

    def user_line(self, frame):
        super(FPdb, self).user_line(frame)
        if self.follow:
            self.do_edit(None, frame=frame)

    def observe_breakpoints(self, breakpoints_file=breakpoints_file):
        event_handler = BreakPointFileModifiedEventHandler(
            self, breakpoints_file)
        observer = Observer()
        observer.schedule(event_handler, os.path.dirname(breakpoints_file))
        observer.start()
        return observer

    def load_breakpoints(self, filename=breakpoints_file):
        print("Reloading breakpoints")
        try:
            self.clear_all_breaks()
        except:
            pass
        breakset = False
        if not os.path.isfile(filename):
            logging.warn("no breakpoints file found at :%s" % filename)
            self.enable_debugger(False)
            return
        with open(filename) as breakfile:
            for line in breakfile:
                line = line.strip()
                if line.startswith('#'):
                    continue
                if not line:
                    continue
                breakset = True
                self.curframe = sys._getframe().f_back
                self.do_break(line)
                self.set_continue()
        if not breakset:
            self.enable_debugger(False)
        else:
            self.enable_debugger()
            self.set_continue()

    def enable_debugger(self, state=True):
        self.skipfirst = 1
        if state:
            frame = sys._getframe()
            self.reset()
            while frame:
                frame.f_trace = self.trace_dispatch
                self.botframe = frame
                frame = frame.f_back
            self._settrace(self.trace_dispatch, self.trace_dispatch)
        else:
            self._settrace(None, None)


    def _settrace(self, func_main, func_thread):
        self.stoplineno = -1
        sys.settrace(func_main)
        threading.settrace(func_thread)

    def set_quit(self):
        self.stopframe = self.botframe
        self.returnframe = None
        self.quitting = 1
        self._settrace(None, None)

    def do_edit(self, arg, frame=None):
        frame = frame or self.curframe
        lineno = frame.f_lineno + 1
        filename = frame.f_code.co_filename
        if os.path.isfile(filename):
            try:
                os.system(editor_cmd % (filename, lineno))
            except:
                print("Editor command failed: %s." % editor_cmd)

    do_e = do_edit

    def do_pprint_edit(self, arg):
        value = pformat(self._getval(arg))
        with NamedTemporaryFile(bufsize=len(value)) as tmpfile:
            tmpfile.write(value)
            tmpfile.flush()
            try:
                os.system(pprint_editor_cmd % tmpfile.name)
            except:
                print("Pprint Editor command failed: %s." % pprint_editor_cmd)

    do_ppe = do_pprint_edit

    def do_pprint_diff(self, arg):
        arg1, arg2 = arg.split(' ')
        value1 = pformat(self._getval(arg1))
        value2 = pformat(self._getval(arg2))
        with NamedTemporaryFile(bufsize=len(value1)) as tmpfile1, \
                NamedTemporaryFile(bufsize=len(value2)) as tmpfile2:
            tmpfile1.write(value1)
            tmpfile2.write(value2)
            tmpfile1.flush()
            tmpfile2.flush()
            try:
                os.system(diff_cmd % (tmpfile1.name, tmpfile2.name))
            except:
                print("Diff Editor command failed: %s." % diff_cmd)

    do_ppd = do_pprint_diff

    def do_search_structure(self, arg):
        """ Recursively Find searchkey inside data structure
        """
        args = arg.split(' ')
        structure = self._getval(args[0])
        searchkey = self._getval(args[1])
        if len(args) > 2:
            level = args[2]
        else:
            level = 0
        if isinstance(structure, dict):
            for key, value in structure.iteritems():
                if self.do_search_structure([key, searchkey, level]) or \
                        self.do_search_structure([value, searchkey, level]):
                    print('-' * level, searchkey, key, ": ", value)
        elif isinstance(structure, list):
            for i, val in enumerate(structure):
                if self.do_search_structure([val, searchkey, level]):
                    print('-' * level, searchkey, repr(structure))
        else:
            if searchkey in str(structure):
                return True
            return False

    do_ss = do_search_structure
    
    def do_follow(self, arg):
        self.follow = not self.follow
        
    def do_shell(self, arg):
        from IPython.frontend.terminal.embed import InteractiveShellEmbed
        shell = InteractiveShellEmbed()  # config=cfg, user_ns=namespace, banner2=banner)
        shell()

    def do_pprintxml(self, arg):
        xml_string = self._getval(arg[0])
        if len(arg) > 1:
            fileout = arg[1]
        else:
            fileout=None
        _xml = xml.dom.minidom.parseString(xml_string)
        strxml = _xml.toprettyxml()
        if fileout:
            with open(os.path.expanduser(fileout), 'w+') as _fileout:
                _fileout.write(strxml)
        print(strxml)

    do_ppx = do_pprintxml

    def do_print_stack(self, arg):
        stack = inspect.stack()
        for st in stack:
            print(st[1:])
            


if __name__ == "__main__":
    try:
        from threading import Thread
        print("one")
        ftrace = FTracer('Linux')
        observer = observe_breakpoints(ftrace, breakpoints_file)
        ftrace.debugger.load_breakpoints()
        ftrace(False)
        print("two no stopping now")
        print("three")
        print("four")
        print("five")
        ftrace.debugger.load_breakpoints()
        print("six")
        print("seven")
        print("eight")
        print("nine")
        print("ten")

        def thread_target():
            print("11")
            print("12")
            print("13")
            print("14")
            print("15")
            print("16")

        t = Thread(target=thread_target, name="TESTTHREAD")
        t.start()
        t.join()
        observer.join()

    except Exception as e:
        logging.exception("Some error")

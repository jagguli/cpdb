classifiers = """\
Development Status :: 2 - Pre-Alpha
Intended Audience :: Developers
License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)
Programming Language :: Python
Topic :: Software Development :: Debuggers
Topic :: Software Development :: Libraries :: Python Modules
Operating System :: Unix
"""
from setuptools import setup, find_packages, Extension

doclines = []
with open("README.md") as readme:
    for line in readme:
        doclines.append(line)

module1 = Extension('_cpdb',
                    sources=['cpdb.c'])

setup(
    name="CPdb",
    version="0.1",
    url='https://bitbucket.org/jagguli/cpdb',
    author='Steven Joseph',
    author_email="steven@stevenjoseph.in",
    packages=['cpdb'],
    install_requires=['setuptools', 'ipython', 'watchdog'],
    license="https://www.gnu.org/copyleft/gpl.html",
    platforms=["unix"],
    description=doclines[0],
    classifiers=filter(None, classifiers.split("\n")),
    long_description=" ".join(doclines[2:]),
    ext_modules=[module1]
)
#include <Python.h>
#include <stdio.h>
#include <frameobject.h>
#include <code.h>
#include <structmember.h>
#include <unicodeobject.h>
/*
* settrace method in c 
*
*/
#define PRINT(obj) PyObject_Print(obj, stdout, Py_PRINT_RAW)
#if PY_MAJOR_VERSION >= 3
#define MOD_ERROR_VAL NULL
#define MOD_SUCCESS_VAL(val) val
#define MOD_INIT(name) PyMODINIT_FUNC PyInit_##name(void)
#define MOD_DEF(ob, name, doc, methods)                         \
    static struct PyModuleDef moduledef = {                     \
        PyModuleDef_HEAD_INIT, name, doc, -1, methods, };       \
    ob = PyModule_Create(&moduledef)
#define PyObject_Compare(obj1, obj2) PyObject_RichCompare(obj1, obj2, Py_EQ)
#define PyInt_FromLong(obj1) PyLong_FromLong(obj1)
#else
#define MOD_ERROR_VAL
#define MOD_SUCCESS_VAL(val)
#define MOD_INIT(name) void init##name(void)
#define MOD_DEF(ob, name, doc, methods)         \
    ob = Py_InitModule3(name, methods, doc)
#define PyUnicode_CompareWithASCIIString(uni, string) PyUnicode_Compare(uni, PyUnicode_FromString(string))
#endif

typedef struct {
    PyObject_HEAD
    PyFrameObject *stopframe;
    PyFrameObject *botframe;
    PyFrameObject *frame_returning;
    PyObject *breaks;
    PyObject *trace;
    PyObject *fncache;

    int quitting;
    int stoplineno;
} CPdb;


static PyObject *
_canonic(CPdb *self, PyObject *filename){
    PyObject *path = PyDict_GetItem(self->fncache, filename);
    if(path == NULL){
        char actualpath [PATH_MAX+1];
        char *filename_str = PyString_AsString(filename);
        char *rpath = realpath(filename_str, actualpath);
        if(rpath != NULL){
            path = PyString_FromString(rpath);
            PyDict_SetItemString(self->fncache, filename_str, path);
        }else{
            path = filename;
        }
    }
    Py_INCREF(path);
    return path;
}

static PyObject *
CPdb_canonic(CPdb *self, PyObject *args){
    PyObject *filename;
    if (!PyArg_ParseTuple(args, "O:canonic", &filename))
        return NULL;
    return _canonic(self, filename);
}

static PyObject *
CPdb_trace_dispatch(CPdb *self, PyObject *args)
{
    PyFrameObject *frame;
    PyObject *event, *arg;
    if(self == NULL || self->quitting != 0){
        Py_RETURN_NONE;
    }
    if (!PyArg_ParseTuple(args, "O!sO:trace_dispatch", &PyFrame_Type, &frame, &event, &arg))
        return NULL;

    PyObject *filename = _canonic(self, frame->f_code->co_filename);
    if(PyMapping_HasKey(self->breaks, filename) == 0 
       && (self->frame_returning == NULL || PyObject_Compare(self->frame_returning, Py_None))){
        Py_INCREF(self->trace);
        return self->trace;
    }

    if(strcmp(event, "line") == 0){
        return PyObject_CallMethod(self, "dispatch_line", "O", frame);
        //return _dispatch_line(self,  frame);
    }else if(strcmp(event, "call") == 0){
        return PyObject_CallMethod(self, "dispatch_call", "OO", frame, arg);
        //return _dispatch_call(self, frame, arg);
    }else if(strcmp(event, "return") == 0){
        return PyObject_CallMethod(self, "dispatch_return", "OO", frame, arg);
        //return _dispatch_return(self,  frame, arg);
    }else if(strcmp(event, "exception") == 0){
        return PyObject_CallMethod(self, "dispatch_exception", "OO", frame, arg);
    }else if(strcmp(event, "c_exception") == 0){
        return PyObject_GetAttrString(self, "trace_dispatch");
    }else if(strcmp(event, "c_call") == 0){
        return PyObject_GetAttrString(self, "trace_dispatch");
    }else if(strcmp(event, "c_return") == 0){
        return PyObject_GetAttrString(self, "trace_dispatch");
    }else{
        Py_INCREF(self->trace);
        return self->trace;
    }
}


static void
CPdb_dealloc(CPdb* self)
{
    Py_XDECREF(self->botframe);
    Py_XDECREF(self->frame_returning);
    Py_XDECREF(self->breaks);
    Py_XDECREF(self->fncache);
    Py_XDECREF(self->trace);
    Py_TYPE(self)->tp_free((PyObject*)self);
}

static PyObject *
CPdb_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    CPdb *self;

    self = (CPdb *)type->tp_alloc(type, 0);
    if (self != NULL) {
        self->quitting = 0;
        self->stoplineno = -1;
        self->trace = NULL;
    }

    return (PyObject *)self;
}

static int
CPdb_init(CPdb *self, PyObject *args, PyObject *kwds)
{
    self->quitting = 0;
    self->breaks = PyDict_New();
    self->fncache = PyDict_New();
    self->trace = PyObject_GetAttrString(self, "trace_dispatch");
    return 0;
}


static PyMemberDef CPdb_members[] = {
    {"quitting", T_INT, offsetof(CPdb, quitting), 0, "cpdb quitting"},
    {"breaks", T_OBJECT_EX, offsetof(CPdb, breaks), 0, "cpdb stopframe"},
    {"fncache", T_OBJECT_EX, offsetof(CPdb, fncache), 0, "cpdb stopframe"},
    {NULL}  /* Sentinel */
};


static PyMethodDef CPdb_methods[] = {
    {"trace_dispatch",  (PyCFunction)CPdb_trace_dispatch, METH_VARARGS,
    "Execute a shell command."},
    {"canonic",  (PyCFunction)CPdb_canonic, METH_VARARGS,
    "canonic"},
    {NULL}  /* Sentinel */
};

static PyTypeObject CPdbType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    "_cpdb.CPdb",             /* tp_name */
    sizeof(CPdb),             /* tp_basicsize */
    0,                         /* tp_itemsize */
    (destructor)CPdb_dealloc, /* tp_dealloc */
    0,                         /* tp_print */
    0,                         /* tp_getattr */
    0,                         /* tp_setattr */
    0,                         /* tp_reserved */
    0,                         /* tp_repr */
    0,                         /* tp_as_number */
    0,                         /* tp_as_sequence */
    0,                         /* tp_as_mapping */
    0,                         /* tp_hash  */
    0,                         /* tp_call */
    0,                         /* tp_str */
    0,                         /* tp_getattro */
    0,                         /* tp_setattro */
    0,                         /* tp_as_buffer */
    Py_TPFLAGS_DEFAULT |
        Py_TPFLAGS_BASETYPE,   /* tp_flags */
    "CPdb objects",           /* tp_doc */
    0,                         /* tp_traverse */
    0,                         /* tp_clear */
    0,                         /* tp_richcompare */
    0,                         /* tp_weaklistoffset */
    0,                         /* tp_iter */
    0,                         /* tp_iternext */
    CPdb_methods,             /* tp_methods */
    CPdb_members,             /* tp_members */
    0,                         /* tp_getset */
    0,                         /* tp_base */
    0,                         /* tp_dict */
    0,                         /* tp_descr_get */
    0,                         /* tp_descr_set */
    0,                         /* tp_dictoffset */
    (initproc)CPdb_init,      /* tp_init */
    0,                         /* tp_alloc */
    CPdb_new,                 /* tp_new */
};


MOD_INIT(_cpdb)
{
    PyObject* m;

    if (PyType_Ready(&CPdbType) < 0)
        return MOD_ERROR_VAL;

    MOD_DEF(m, "_cpdb", "c extensions for pdb", NULL);
    if (m == NULL)
        return MOD_ERROR_VAL;

    Py_INCREF(&CPdbType);
    PyModule_AddObject(m, "CPdb", (PyObject *)&CPdbType);
    return MOD_SUCCESS_VAL(m);
}
